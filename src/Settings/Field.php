<?php
namespace Offtic\wpcommons\Settings;

use Offtic\wpcommons\Context;

class Field
{
    var $field;
    
    /**
     * 
     * @param array $field array(
     *      'name' =>
     *      'label' =>
     *      'default' => 
     *      'description'
     * )
     */
    function __construct( $field ) {
        $this->field = $field;
    }
}

