<?php
namespace Offtic\wpcommons\Settings;

use Offtic\wpcommons\Context;

class Section
{
    var $id;
    var $title;
    var $description; 
    var $page;
    /**
     * 
     * @var Context
     */
    var $context;
    
    /**
     * 
     * @var Field[]
     */
    var $fields = array();
    
    function __construct( Context $context, string $id, string $title, string $description, string $page) {
        $this->context = $context;
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->page = $page;
    }
    
    function add_field( Field $field ) {
        $this->fields[] = $field;
    }
    
    function render( ) {
        add_settings_section( $this->id,  $this->title, array($this, 'description'), $this->page );
        foreach ( $this->fields as $field ) {
            add_settings_field(
                $this->context->plugin_name . '_setting_' . $field->field['name'],
                __( $field->field['label'], $this->context->plugin_name ),
                array( $this, 'render_field'),
                $this->context->plugin_name,
                $this->id, $field->field );
        }
    }
    
    function description(){
        echo $this->description;
    }
    
    function render_field( $field ) {
        $options = $this->context->get_options();
        $value = $field['default'];
        if ( $options[$field['name']] ) {
            $value = $options[$field['name']];
        }
        if ( isset($field['options']) ) {
            $hoptions = "";
            foreach ( $field['options'] as $key => $option ) {
                $selected = ($value == $key ? 'selected' : '');
                $hoptions .= "<option value='$key' $selected>$option</option>";
            }
            echo "<select name='" . $this->context->plugin_name . "_options[{$field['name']}]'>$hoptions</select>";
        } else {
            echo "<input name='" . $this->context->plugin_name . "_options[{$field['name']}]' type='text' value='$value' />";
        }
        echo "<p class='description' id='{$field['name']}-description'>{$field['description']}</p>";
    }
}

