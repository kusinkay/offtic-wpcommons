<?php
namespace Offtic\wpcommons;

use Offtic\wpcommons\Settings\Section;
use Offtic\wpcommons\Settings\Field;

class Settings
{
    /**
     * 
     * @var Context
     */
    private $context;
    
    /**
     * 
     * @var Section[]
     */
    private $sections = array();
    
    function __construct( Context $context ) {
        $this->context = $context;
        
        /***
         * Default required settings
         */
        $this->default();
        
        add_action( 'admin_menu', array($this, 'add_settings_page' ) );
        add_action( 'admin_init', array($this, 'register_settings' ) );
    }
    
    function add_section( Section $section ) {
        $this->sections[] = $section;
    }
    
    function add_settings_page() {
        add_options_page(
            sprintf(__( '%s settings', Context::OWPC_COMMON_DOMAIN), $this->context->title ),
            sprintf(__( '%s settings', Context::OWPC_COMMON_DOMAIN), $this->context->title ),
            'manage_options',
            $this->context->plugin_name,
            array( $this, 'render_plugin_settings_page') );
    }
    
    function render_plugin_settings_page() {
        ?>
    	    <h2><?php echo sprintf(__('%s settings', $this->context->plugin_name ), $this->context->title)?></h2>
    	    <form action="options.php" method="post">
    	        <?php 
    	        settings_fields( $this->context->plugin_name . '_options' );
    	        do_settings_sections( $this->context->plugin_name ); ?>
    	        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    	    </form>
    	    <?php
    }
    
    function register_settings() {
        register_setting( $this->context->plugin_name . '_options', $this->context->plugin_name. '_options' /*, $this->context->plugin_name . '_options_validate'*/ );
        foreach ($this->sections as $section ) {
            $section->render();
        }
    }
    
    function default() {
        $section = new Section(
            $this->context,
            'owpc_devel',
            __('Devel options', Context::OWPC_COMMON_DOMAIN),
            __('Options for developers and debugers', Context::OWPC_COMMON_DOMAIN),
            $this->context->plugin_name);
        {
            $section->add_field(new Field(array(
                'name' => 'logthreshold',
                'label' => __('Log level', Context::OWPC_COMMON_DOMAIN),
                'default' => '0',
                'description' => __('To debug errors and review them in apache logs. It will show levels downwards the options.', Context::OWPC_COMMON_DOMAIN),
                'options' => array(
                    '0' => 'DEBUG',
                    '1' => 'WARNING',
                    '2' => 'INFO',
                    '3' => 'ERROR',
                    '4' => 'FATAL',
                )
            )));
        }
        $this->add_section($section);
    }
}

