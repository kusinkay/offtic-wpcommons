/// <reference path="../../../vendor/zurb/foundation/dist/js/foundation.d.ts" />
declare module Offtic.WPCommons {
    class Utils {
        /**
         * If len is greater than array.length, there could be repeating items
         */
        static rand(array: Array<object>, len?: number): object[];
        /**
         * recursive: current config node appending the suffix
         */
        static walk(node: object, field: object, ignoreEmpty?: boolean): object;
        static format(text: string, args: string[]): string;
    }
}
declare module Offtic.WPCommons {
    function success(text: any, container?: string): void;
    function warning(text: any, container?: string): void;
    function alert(text: any, container?: string): void;
    function confirm(text: string, yes: any, no?: any, end?: any): boolean;
}
declare module Offtic.WPCommons {
    abstract class View {
        abstract declareListeners(): void;
        protected viewName: string;
        protected literals: any;
        protected captchaFields: Array<string>;
        constructor();
        abstract setViewName(): any;
        /**
         * Mainly used by a module to import literals from the template
         *
         * @param key
         * @param value
         */
        addLiteral(key: string, value: string): void;
        text(key: string, args?: string[]): any;
        fieldsToParams(selectors: Array<string>): object;
        uncheck(form: any): void;
        toDbDate(date: string): string;
        fromDbDate(date: string): string;
        placeContent(template: string, container: string, overwrite?: boolean): void;
        listItem(dest: string, href: string, label: string, icon: string, target?: string, xtricons?: Array<string>): void;
        /**
         * Template must have one form tag with class=view-form
         * @param action
         * @param params
         * @param success
         * @param error
         * @param complete
         */
        backofficeCall(action: string, params: object, success: any, error?: any, complete?: any): void;
        showFace(face: string): void;
        setNonce(field: string): void;
        showLoading(): void;
        hideLoading(): void;
        buttonStarted($el: any): void;
        buttonStopped($el: any): void;
        traceEvent(action: string, label?: string, value?: number): void;
        protected addCaptchaFields(fields: Array<string>): void;
    }
}
