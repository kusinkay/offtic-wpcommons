<!--
REVEALS
-->
<div class="reveal" id="ofwc-confirm" data-reveal
	data-close-on-click="false" data-close-on-esc="false">
	<div class="text-center">
		<h4 id="ofwc-confirmText">{Text}</h4>
	</div>
	<div class="grid-x">
		<div class="cell small-6 text-right">
			<button type="button" class="button alert" id="confirmNo"
				data-i18n="option.confirm.no">{no}</button>
		</div>
		<div class="cell small-6 text-left">
			<button type="button" class="button success" id="confirmYes"
				data-i18n="option.confirm.yes">{yes}</button>
		</div>
	</div>
	
</div>
<div id="ofwc-message-container"></div>
<div class="reveal large" id="ofwc-reveal-rgpd-nospam" data-reveal
	data-close-on-click="true" data-close-on-esc="true" data-multiple-opened="true">
	<div class="row">
		<h4>Cláusulas legales</h4>
		<?php echo $class_ref->get_rgpd_nospam();?>
	</div>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="reveal large" id="ofwc-reveal-rgpd-spam" data-reveal
	data-close-on-click="true" data-close-on-esc="true" data-multiple-opened="true">
	<div class="row">
		<h4>Cláusulas legales</h4>
		<?php echo $class_ref->get_rgpd_spam();?>
	</div>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div id="ofwc-loading" class="tiny reveal" data-reveal data-close-on-click="true" data-animation-in="fade-in" role="dialog" aria-hidden="true" data-yeti-box="umc_modal_container" data-resize="umc_modal_container" data-events="resize" style="top: 36px;">
	<div class="text-center"><div class="fa fa-spinner fa-pulse fa-2x fa-fw"></div></div>
</div>
