<?php
namespace Offtic\wpcommons;
/**
 * Utilities that must be plugin-contextualized in order to be shared among multiple plugins
 * @author Juane Puig
 *
 */
class Context
{
    var $acronim;
    var $plugin_name;
    var $plugin_file;
    var $namespace;
    var $title;
    
    /**
     * 
     * @var Logger
     */
    var $logger;
    
    private $raw_modules = array();
    
    /**
     * 
     * @var Module[]
     */
    private $modules = array();
    
    /**
     * 
     * @var Controller[]
     */
    private $controllers = array();
    
    private $default_templates = FALSE;
    
    private $styles = array();
    
    private $scripts = array();
    
    const OWPC_COMMON_DOMAIN = 'offtic-wpcommons';
    
    const VENDOR_PATH = '/vendor/offtic/wpcommons/src/';
    
    /**
     * 
     * @param string $acronim short id, recommended three chars 
     * @param string $plugin_name
     * @param string $plugin_file __FILE__ from the main file
     * @param string $namespace Namespace where modules and controllers are declared (format '\\Package\\Subpackage'
     */
    public function __construct( $acronim, $plugin_name, $plugin_file = __FILE__, $namespace = '', $title = NULL ) {
        
        $this->acronim = $acronim;
        $this->plugin_name = $plugin_name;
        $this->plugin_file = $plugin_file;
        $this->namespace = $namespace;
        $this->title = ( $title == NULL ? $plugin_name : $title );
        
        $this->logger = new Logger($this);
    }
    
    public function add_module( $module ) {
        $this->raw_modules[] = $module;
    }
    
    public function init () {
        
        foreach( $this->raw_modules as $module ){
            if (file_exists(dirname( $this->plugin_file ) . '/includes/Modules/' . $module . 'Module.php') ) {
                require_once dirname( $this->plugin_file ) . '/includes/Modules/' . $module . 'Module.php';
                $class =  $this->namespace . '\\Modules\\' . $module . 'Module';
                if ( class_exists( $class )) {
                    $this->modules[$module] = new $class($this);
                    $this->logger->debug('Module class for ' . $module . ' found');
                }
            }
            if (file_exists(dirname( $this->plugin_file ) . '/includes/Controllers/' . $module . 'Controller.php') ) {
                require_once dirname( $this->plugin_file ) . '/includes/Controllers/' . $module . 'Controller.php';
                $class =  $this->namespace . '\\Controllers\\' . $module . 'Controller';
                if ( class_exists( $class )) {
                    $this->controllers[$module] = new $class($this);
                    $this->logger->debug('Controller class for ' . $module . ' found');
                } else {
                    $this->logger->debug('No controller class for ' . $module);
                }
            } else {
                $this->logger->debug('No controller for ' . $module);
            }
        }
        
        foreach ( $this->modules as $module ) {
            $module->init();
        }
        add_action( 'wp_enqueue_scripts', array($this, 'load_scripts') );
    }
    
    function load_translation_files() {
        $lang_dir = dirname( $this->plugin_file ) . '/languages/';
        //load_plugin_textdomain(UMC_PLUGIN_NAME, false, $lang_dir);
        load_textdomain($this->plugin_name, $lang_dir . get_locale() . '.mo');
    }
    
    function push_script( $key, $path ) {
        $this->scripts[] = array(
            'key' => $key,
            'path' => $path
        );
    }
    
    function push_style( $key, $path ) {
        $this->styles[] = array(
            'key' => $key,
            'path' => $path
        );
    }
    
    function load_clean_script( $handle, $src ) {
        $filename = dirname($this->plugin_file) . $src;
        $ts = date ("YmdHis", filemtime($filename));
        wp_enqueue_script( $handle, plugins_url( $src,  $this->plugin_file ) . "?ts=" . $ts);
    }
    
    function load_clean_style( $handle, $src ) {
        $filename = dirname($this->plugin_file) . $src;
        $ts = date ("YmdHis", filemtime($filename));
        wp_register_style( $handle, plugins_url( $src,  $this->plugin_file ) . "?ts=" . $ts);
    }
    
    function load_scripts() {
        $this->load_clean_style( $this->acronim . '_app_css', 		'/assets/css/app.css' );
        wp_enqueue_style( $this->acronim . '_app_css' );
        
        add_action( 'wp_enqueue_scripts', array($this, 'unload_scripts'), 11 );
        $this->load_clean_script($this->acronim . '_jquery_js', '/vendor/components/jquery/jquery.min.js');
        $this->load_clean_style($this->acronim . '_foundation_css', '/vendor/zurb/foundation/dist/css/foundation.min.css');
        $this->load_clean_script($this->acronim . '_foundation_js', '/vendor/zurb/foundation/dist/js/foundation.min.js');
        
        $this->logger->debug('styles: ' . json_encode($this->styles));
        
        foreach ( $this->styles as $style ) {
            $this->load_clean_style($this->acronim . '_' . $style['key'], $style['path'] );
        }
        foreach ( $this->scripts as $script ) {
            $this->load_clean_script($this->acronim . '_' . $script['key'], $script['path'] );
        }
    }
    
    function unload_scripts() {
        wp_deregister_script('jquery');
        wp_deregister_script('jquery-ui-core');
    }
    
    function get_nonce_action() {
        return $this->acronim . '_nonce_action';
    }
    
    function get_options() {
        return get_option( $this->plugin_name . '_options' );
    }
    
    function get_page_id ($key) {
        $name = $this->acronim . '_' . $key . '_defpage_id';
        $value = get_option( $name );
        if ( !isset($value) || empty($value)){
            return FALSE;
        }
        return $value;
    }
    
    function set_page_id ($key, $id) {
        $name = $this->acronim . '_' . $key . '_defpage_id';
        update_option($name , $id);
    }
    
    /**
     * Reads the content from a file inside the plugin folder
     * @param string $filename
     */
    function get_filecontent( $filename ) {
        return file_get_contents( dirname($this->plugin_file) . $filename);
    }
    
    function get_template ( $template, Module $class_ref, $from_common = FALSE ) {
        ob_start();
        $plugin_path = '/assets/templates/';
        $common_path = self::VENDOR_PATH . 'assets/templates/';
        $path = $plugin_path;
        if ( $from_common ) $path = $common_path;
        include dirname( $this->plugin_file ) . $path . $template;
        $content = '';
        if ($this->default_templates === FALSE) {
            $file = dirname( $this->plugin_file ) . $common_path. 'templates.html';
            $content .= file_get_contents( $file );
            $this->default_templates = TRUE;
        }
        return ob_get_clean() . $content;
    }
}

