module Offtic.WPCommons {
    export class Utils {
        /**
         * If len is greater than array.length, there could be repeating items
         */
        public static rand( array: Array<object>, len?: number ) {
            var assignat: Array<object> = new Array<object>();
            if ( len != null && len != array.length ) {
                for ( var i = 0; i < len; i++ ) {
                    var r = Math.floor(( Math.random() * ( array.length ) - 1 ) + 1 );
                    assignat[i] = array[r];
                }
            } else {
                for ( var i = 0; i < array.length; i++ ) {

                    var r = Math.floor(( Math.random() * ( array.length ) - 1 ) + 1 );
                    while ( assignat[r] != undefined ) {
                        r = Math.floor(( Math.random() * ( array.length ) - 1 ) + 1 );
                    }
                    assignat[r] = array[i];
                }
            }
            return assignat;
        }

        /**
         * recursive: current config node appending the suffix
         */
        public static walk( node: object, field: object, ignoreEmpty?: boolean ): object {
            var pos = field['name'].indexOf( "." );
            if ( pos > -1 ) {
                var current = field['name'].substring( 0, pos );
                var rest = field['name'].substring( pos + 1 );
                if ( node[current] == null ) {
                    node[current] = {};
                }
                node[current] = Utils.walk( node[current], { "name": rest, "value": field['value'] }, ignoreEmpty );
            } else {
                /*is leave: scalar value*/
                node[field['name']] = field['value'];
                if ( ( field['value'] == "" || field['value'] == null ) && ignoreEmpty ) {
                    delete node[field['name']];
                }
            }
            return node;
        }

        public static format( text: string, args: string[] ) {
            return text.replace( /{(\d+)}/g, function( match, number ) {
                return typeof args[number] != 'undefined'
                    ? args[number]
                    : match;
            } );
        }
    }
}