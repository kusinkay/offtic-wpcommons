module Offtic.WPCommons {
    var messageIndex = 0;
    var messageItvs = [];
    export function success( text, container?: string ) {
        showMessage( text, "success", container );
    }
    export function warning( text, container?: string ) {
        showMessage( text, "warning", container );
    }
    export function alert( text, container?: string ) {
        showMessage( text, "alert", container );
    }

    function showMessage( text, type, container?: string ) {
        if ( container == null ) {
            container = "#ofwc-message-container";
        }
        var callout = jQuery( "#tpl_message > div:first" ).clone( true );
        jQuery( callout ).addClass( type );
        jQuery( callout ).attr( "id", "message-" + messageIndex );
        jQuery( "span:first", callout ).html( text );
        jQuery( container ).append( callout );
        var currentIndex = messageIndex;
        messageItvs.push( setInterval( function() {
            jQuery( "#message-" + currentIndex ).remove();
            clearInterval( messageItvs[currentIndex] );
        }, 3000 ) );
        messageIndex++;
    }

    export function confirm( text: string, yes, no?, end?) {
        jQuery( "#confirm" ).foundation( "open" );
        jQuery( "#confirmText" ).html( text );
        jQuery( '#confirmYes' ).unbind( 'click' );
        jQuery( "#confirmYes" ).click( function( event ) {
            event.preventDefault();
            yes();
            if ( typeof end == "function" )
                end();
            jQuery( "#confirm" ).foundation( "close" );
        } );
        jQuery( '#confirmNo' ).unbind( 'click' );
        jQuery( "#confirmNo" ).click( function( event ) {
            event.preventDefault();
            if ( typeof no == "function" ) {
                no();
                if ( typeof end == "function" )
                    end();
            }
            jQuery( "#confirm" ).foundation( "close" );
        } );
        if ( yes == null ) {
            jQuery( "#confirmText" ).html( "ERROR: No action taken on Accept, please use the correct callbacks" );
            return false;
        }
    }
}