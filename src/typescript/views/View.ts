/// <reference path='../../../vendor/zurb/foundation/dist/js/foundation.d.ts' />
module Offtic.WPCommons {
    export abstract class View {

        abstract declareListeners(): void;

        protected viewName: string;

        protected literals: any = {};

        protected captchaFields: Array<string>;

        constructor() {

            var self = this;

            this.setViewName();

            /**
              * Validadores custom
              */
            function rangeValidator(
                $el,      /* jQuery element to validate */
                required, /* is the element required according to the `[required]` attribute */
                parent    /* parent of the jQuery element `$el` */
            ) {
                if ( !required ) return true;
                var min = parseInt( $el.attr( 'min' ) );
                var max = parseInt( $el.attr( 'max' ) );

                var value = $el.val();
                return ( value >= min && value <= max );
            };

            // Set default options
            Foundation.Abide.defaults.validators['range'] = rangeValidator;

            // Initialize Foundation
            jQuery( document ).foundation();
            /*  Validadores custom */



            jQuery( '[data-checkbox-validator] [type="checkbox"]' ).click( function() {
                var target = jQuery( this ).attr( 'data-target' );
                var $hidden = jQuery( '#' + target );
                var checked = jQuery( this ).prop( 'checked' );
                $hidden.val(( checked ? "somevalue" : "" ) );
            } );

            this.declareListeners();

            jQuery( '[data-view]' ).show();

            jQuery( '[data-open-face]' ).click( function( event ) {
                event.preventDefault();
                self.showFace( jQuery( this ).attr( 'data-open-face' ) );
            } );

        }

        abstract setViewName();

        /**
         * Mainly used by a module to import literals from the template
         * 
         * @param key
         * @param value
         */
        addLiteral( key: string, value: string ) {
            this.literals[key] = value;
        }

        text( key: string, args?: string[] ) {
            if ( args != null ) {
                return Offtic.WPCommons.Utils.format(this.literals[key], args);
            }
            return this.literals[key];
        }

        fieldsToParams( selectors: Array<string> ): object {
            var classRef = this;
            var result: object = {};
            for ( var i = 0; i < selectors.length; i++ ) {
                jQuery( selectors[i] ).each( function() {
                    var value = jQuery( this ).val();
                    var name = jQuery( this ).attr( 'name' );
                    if ( jQuery( this ).attr( 'pattern' ) == 'day_month_year' ) {
                        //its a date, change format
                        value = classRef.toDbDate( value );
                    } else if ( jQuery( this ).attr( 'type' ) == 'radio' ) {
                        //radio, let's find checked

                    } else if ( jQuery( this ).attr( 'type' ) == 'checkbox' ) {
                        //checkbox, let's find checked
                        if ( jQuery( this ).prop( 'checked' ) ) {
                            value = jQuery( this ).val();
                        } else {
                            value = null;
                        }
                    }

                    var field = {
                        name: name,
                        value: value
                    }
                    var ignoreEmpty = true;
                    result = Offtic.WPCommons.Utils.walk( result, field, ignoreEmpty );

                } );
            }
            return result;
        }

        uncheck( form ) {
            jQuery( form + ' input[type=checkbox], ' + form + ' input[type=radio]' ).each( function() {
                jQuery( this ).prop( 'checked', false );
            } );
        }

        toDbDate( date: string ): string {
            var result = "";
            var parts = date.split( '/' );
            parts.reverse();
            return parts.join( '-' );
            return result;
        }

        fromDbDate( date: string ): string {
            date = date.substr( 0, 10 ); //quitamos time
            var result = "";
            var parts = date.split( '-' );
            parts.reverse();
            return parts.join( '/' );
            return result;
        }

        placeContent( template: string, container: string, overwrite?: boolean ) {
            if ( overwrite == null ) {
                overwrite = true;
            }
            if ( overwrite ) {
                jQuery( '#' + container ).html( '' );
            }
            var div = jQuery( '#ofwc-templates #' + template + ' div:first-child' ).clone( true );
            jQuery( '#' + container ).append( div );
        }

        listItem( dest: string, href: string, label: string, icon: string, target?: string, xtricons?: Array<string> ) {
            var li = jQuery( '#ofwc-templates #tpl_listed_item li:first-child' ).clone( true );
            jQuery( 'a', li ).attr( 'href', href );
            jQuery( 'a', li ).html( label );
            if ( target != null ) {
                jQuery( 'a', li ).attr( 'target', target );
            }
            jQuery( '.bullet.fa', li ).addClass( icon );
            if ( xtricons != null ) {
                for ( var i = 0; i < xtricons.length; i++ ) {
                    jQuery( 'a', li ).append( '&nbsp;' );
                    var jqicon = jQuery( '<i class="fa" aria-hidden="true">' );
                    jQuery( jqicon ).addClass( xtricons[i] );
                    jQuery( 'a', li ).append( jqicon );
                }
            }
            jQuery( dest ).append( li );
        }

        /**
         * Template must have one form tag with class=view-form
         * @param action
         * @param params
         * @param success
         * @param error
         * @param complete
         */
        backofficeCall( action: string, params: object, success, error?, complete?) {
            var data = {
                'action': action, // your action name
                '_wpnonce': jQuery( "[name='_wpnonce']" ).val(),
                '_wp_http_referer': jQuery( "[name='_wp_http_referer']" ).val(),
            };

            if ( params != null ) {
                for ( var key of Object.keys( params ) ) {
                    data[key] = params[key];
                }
            }

            jQuery.ajax( {
                url: backoffice.ajaxurl, // this will point to admin-ajax.php
                type: 'POST',
                data: data,
                success: function( response ) {
                    if ( typeof success == 'function' ) {
                        success( response );
                    }
                },
                error: function() {
                    if ( typeof error == 'function' ) {
                        error();
                    }
                },
                complete: function() {
                    if ( typeof complete == 'function' ) {
                        complete();
                    }
                }
            } );
        }

        showFace( face: string ) {
            jQuery( '[data-face]' ).removeClass( 'is-active' );
            jQuery( '#' + face + '[data-face]' ).addClass( 'is-active' );

            jQuery( [document.documentElement, document.body] ).animate( {
                scrollTop: jQuery( "#" + face ).offset().top - 100
            }, 500 );
        }

        setNonce( field: string ) {
            var nonce = jQuery( field );
            jQuery( ".view-form" ).append( nonce );
        }

        showLoading() {
            if ( jQuery( '#ofwc-loading' ).length > 0 ) {
                jQuery( '#ofwc-loading' ).foundation( 'open' );
            } else {
                console.error( 'No loading reveal. Have you called add_reveals method from your module?' );
            }
        }

        hideLoading() {
            if ( jQuery( '#ofwc-loading' ).length > 0 ) {
                jQuery( '#ofwc-loading' ).foundation( 'close' );
            } else {
                console.error( 'No loading reveal. Have you called add_reveals method from your module?' );
            }
        }

        buttonStarted( $el ) {
            if ( jQuery( '[data-loading]', $el ).length == 0 ) {
                var loading = jQuery( '#ofwc-templates > #ofwc-tpl_loading > div:first-child' ).clone( true );
                $el.append( loading );
            }
        }
        
        buttonStopped( $el ) {
            jQuery( '[data-loading]', $el ).remove();
        }

        traceEvent( action: string, label?: string, value?: number ) {
            if ( typeof gtag == 'function' ) {
                var tag = {
                    'event_category': this.viewName,
                    'event_label': label
                };
                if ( value != null ) {
                    tag['value'] = value;
                }
                gtag( action, tag );
            }
        }

        protected addCaptchaFields( fields: Array<string> ) {
            this.captchaFields = fields;
        }
    }
}