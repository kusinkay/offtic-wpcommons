<?php
namespace Offtic\wpcommons;

abstract class Module
{
    /**
     *
     * @var Logger
     */
    protected $logger;
    
    /**
     * 
     * @var Context
     */
    protected $context;
    
    protected $errors = array();
    
    protected $messages = array();
    
    /**
     * To transfer i18n WordPress system to javascript environment
     * @var array
     */
    public $literals = array();
    
    protected $content = '';
    
    protected $js = '';
    
    private $pages = array();
    
    function __construct( Context $context ){
        $this->logger = new Logger( $context, __CLASS__ );
        $this->context = $context;
        
        add_action( 'wp_enqueue_scripts', array($this, 'load_common_scripts') );
    }
    
    function init() {
        $this->add_shortcodes();
        $this->add_pages();
        
        add_filter( 'display_post_states', array( $this, 'add_display_post_states' ), 10, 2 );
        
    }
    
    abstract function add_shortcodes();
    
    abstract function add_pages();
    
    function add_page( $key, $title, $slug, $shortcode ) {
        if (is_admin()) {
            $this->pages[] = array(
                'key' => $key,
                'title' => $title,
                'slug' => $slug
            );
            
            $post_id = $this->context->get_page_id($key);
            if ( $post_id === FALSE ) {
                //No existia, la creamos
                $my_post = array(
                    'post_title'   => $title,
                    'post_content' => $shortcode,
                    'post_type'    => 'page',
                    'post_status'  => 'publish',
                    'post_name'    => $slug
                );
                // Update the post into the database
                $post_id = wp_insert_post( $my_post, true );
                if (is_wp_error($post_id)) {
                    $this->logger->warning( __METHOD__ . ': Could not create page '. $key);
                    $errors = $post_id->get_error_messages();
                    foreach ($errors as $error) {
                        $this->logger->warning( __METHOD__ . ': ' . $error);
                    }
                } else {
                    $this->context->set_page_id($key, $post_id);
                    $this->logger->debug( "Página '$key' creada: $post_id" );
                }
            } else {
                $this->logger->debug( "La página '$key' ya existia: $post_id" );
            }
        }
    }
    
    function add_page_CLEAN( $key, $title, $slug, $shortcode ) {
        $post_id = $this->context->get_page_id($key);
        if ( $post_id !== FALSE ) {
            wp_delete_post($post_id, true);
        }
        $this->context->set_page_id($key, NULL);
    }
    
    function add_template( $template ) {
        $this->content .= $this->context->get_template($template, $this);
    }
    
    
    function add_reveals () {
        $this->content .= $this->context->get_template('reveals.php', $this, TRUE);
    }
    
    function add_instruction ( $js ) {
        $this->js .= $js;
    }
    
    function run( $view_name ) {
        $js_module = implode('.',  array_slice(explode('\\', $this->context->namespace), 1)  );
        
        $literals = '';
        foreach ( $this->literals as $key => $value ) {
            $literals .= "oView.addLiteral('$key', '$value');";
        }
        
        return "
            $this->content
            <script>
                var oView;
				jQuery(document).ready(function(){
					oView = new {$js_module}." . ucfirst($view_name) . "View() ;
                    oView.setNonce('" . wp_nonce_field( $this->context->get_nonce_action() ) . "');
                    {$this->js}
                    $literals
				});
			</script>
			";
    }
    
    /**
     * Para sobrescribir en cada modulo
     */
    function plugins_loaded() {
        
        /**
         * Captcha
         */
        $options = $this->context->get_options();
        
        if ( $options['captcha_enabled'] === "1") {
            if ( function_exists( 'gglcptch_display' ) ) {
                add_filter( $this->context->acronim . '_contact_captcha', function(){ return gglcptch_display();} );
                add_filter( $this->context->acronim . '_captcha_fields', function(){ return 'UmcWordPress.view.addCaptchaFields(["#g-recaptcha-response"])';} );
            } else {
                $this->logger->info( 'gglcptch_commentform_display not available!!!');
            }
        }
        /* captcha */
    }
    
    function load_common_scripts() {
        $this->context->load_clean_script('offtic_wpcommons', Context::VENDOR_PATH . 'assets/js/wpcommons.js');
        $tail = implode('', array_slice(explode('\\', $this->context->namespace ), -1));
        $handle = 'offtic_' . $this->context->plugin_name;
        $this->context->load_clean_script($handle, '/assets/js/' . strtolower($tail) . '.js');
        wp_localize_script($handle, "backoffice",  array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }
    
    /**
     * Add a post display state for special UMC pages in the page list table.
     *
     * @param array   $post_states An array of post display states.
     * @param object $post        The current post object.
     */
    public function add_display_post_states( $post_states, $post ) {
        foreach ( $this->pages as $page ){
            if ( $this->context->get_page_id( $page['key'] ) == $post->ID ) {
                //translators: required text will print page title
                $post_states[$this->context->acronim . '_page_for_' . $page['key']] = sprintf( __( 'Page for &quot;%s&quot;', $this->context->plugin_name ), $page['title'] );
            }
        }
        return $post_states;
    }
    
    protected function display_error( $message ) {
        return '<div class="callout alert">
              <p>' . $message . '</p>
            </div>';
    }
    
    /**
     * reserved for RGPD
     * @return string
     */
    function get_rgpd_nospam() {
        return '';
    }
    
    /**
    * reserved for RGPD
    * @return string
    */
    function get_rgpd_spam() {
        return '';
    }
    
    function get_captcha() {
        return apply_filters( $this->context->acronim . '_contact_captcha', '');
    }
    
    function add_error( $error ) {
        $this->errors[] = $error;
    }
    
    function get_errors() {
        return $this->errors;
    }
}

