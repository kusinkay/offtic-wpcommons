<?php
namespace Offtic\wpcommons;

class Controller
{
    
    
    protected $methods = array();
    
    /**
     * 
     * @var Context
     */
    protected $context;
    
    function __construct ( Context $context ) {
        $this->context = $context;
        foreach ( $this->methods as $method ) {
            add_action( 'wp_ajax_' . $this->context->acronim . '_' . $method,  array( $this, $method) );
            add_action( 'wp_ajax_nopriv_' . $this->context->acronim . '_' . $method, array( $this, $method) );
        }
    }
    
    protected function response( callable $function ) {
        if ( check_ajax_referer( $this->context->get_nonce_action() ) !== false ) {
            header("Content-type: application/json");
            $result = array();
            $result = $function( $this );
            echo json_encode($result);
        }
        wp_die();
    }
    
    protected function check_captcha() {
        $output = array("status" => "err");
        $options = $this->context->get_options();
        if ( $options['captcha_enabled'] === "1") {
            if ( function_exists( 'gglcptch_check' ) ) {
                $captchck = gglcptch_check();
                if ( !empty($captchck) &&  $captchck['response'] === FALSE ) {
                    $output['message'] = __( "Please verify captcha", $this->context->plugin_name );
                    $this->context->logger->error('Catcha invalid: ' . $captchck['reason']);
                    return $output;
                }
            } else {
                $this->context->logger->warning('No captcha Check available');
            }
        }
        return TRUE;
    }
}

