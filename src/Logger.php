<?php
namespace Offtic\wpcommons;

class Logger
{
    const DEBUG = 0;
    const WARNING = 1;
    const INFO = 2;
    const ERROR = 3;
    const FATAL = 4;
    
    var $level = self::FATAL;
    var $prefix = "";
    
    
    public function __construct( Context $context, $prefix = "" ) {
        $this->prefix = $prefix;
        $options = $context->get_options();
        if ( isset($options['logthreshold'])) {
            $this->level = intval( $options['logthreshold'] );
        }
    }
    
    public function fatal( $message ) {
        $this->log($message, self::FATAL);
    }
    
    public function error( $message ) {
        $this->log($message, self::ERROR);
    }
    
    public function warning( $message ) {
        $this->log($message, self::WARNING);
    }
    
    public function info( $message ) {
        $this->log($message, self::INFO);
    }
    
    public function debug( $message ) {
        $this->log($message, self::DEBUG);
    }
    
    private function log( $message, $level ) {
        if ( $this->level <= $level ) {
            $label = "";
            switch($level) {
                case self::DEBUG:   $label = "[DEBUG] "; break;
                case self::WARNING: $label = "[WARNING] "; break;
                case self::INFO:    $label = "[INFO] "; break;
                case self::ERROR:   $label = "[ERROR] "; break;
                case self::FATAL:   $label = "[FATAL] "; break;
            }
            error_log( $this->prefix . ' >>> ' . $label . $message);
        }
    }
}

