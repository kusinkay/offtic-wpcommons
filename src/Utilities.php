<?php
namespace Offtic\wpcommons;

class Utilities
{
    static function dateFromDb( $dbdate, $format = 'j/M/Y' ) {
        return date( $format, strtotime($dbdate) );
    }
}

