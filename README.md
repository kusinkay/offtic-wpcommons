# Offtic WpCommons
Library to prevents to invent the weel over and over again regarding to implement WorPress Plugins

## Usage
Main file

```
if (! defined ( 'OMMS_PLUGIN_NAME' )) {
    define ( 'OMMS_PLUGIN_NAME', 'my-meals-scheduler' );
}
if (! defined ( 'OMMS_PLUGIN_FILE' )) {
    define ( 'OMMS_PLUGIN_FILE', __FILE__ );
}

$context = new Context('omms', OMMS_PLUGIN_NAME, OMMS_PLUGIN_FILE, '\\Offtic\\MyMealsScheduler');
$context->add_module('YouNameIt');
$context->add_module('SecondOne');

add_action( 'init', array($context, 'init') );
add_action( 'plugins_loaded', array($context, 'load_translation_files') );
```

## File structure
- includes --- classes under the context defined namespace, example Offtic\MyMealsScheduler
	- Modules --- contains classes that extend ---> Offtic\wpcommons\Module
		- YouNameItModule.php
		- SecondOneModule.php
	- Controllers --- contains classes that extend ---> Offtic\wpcommons\Component
- assets
	- templates --- load them by Context::add_template within shortcode function
		- younameit.php
		- secondone.php
- typescript
	- views 
		- classes' module must match the namespace parameter of the context. Example Offtic.MyMealsScheduler
		- classes' name must have <<Function>>View as name, Where Function is the shortcode function
	- tsconfig.json -- its output js file must have last package name (sample mymealsscheduler.js)
		
## Addressing data from templates
Module-extended classes' attributes and methods can be accessed from within the templates using the intantiated object $class_ref

```
$var1 = $class_ref->attr1;
echo $class_ref->func1();
```
## Accessing WordPress translation system from typescript
Declare literals from templates

```
$class_ref->literals['key1'] = __('Some sample text');
$class_ref->literals['key2'] = __('Some alternative sample text');
```

and the refere to them in typecript View class

```
var t1 = this.text('key1');
var t2 = this.text('key2');
```

## Ajax supported view template requirements
- It must have one form tag with class=view-form